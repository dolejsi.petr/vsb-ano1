// ANO1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "shape.h"

void treshold(cv::Mat &source, cv::Mat &thresholded, uchar thresholding_value);
void indexObjects(cv::Mat &thresholded, cv::Mat &indexed, std::vector<Shape> &shapes);
void colorObjects(cv::Mat &indexed, cv::Mat &colorized, std::vector<Shape> &shapes);
void moments(cv::Mat &indexed, std::vector<Shape> &shapes);

int main()
{
	cv::Mat source = cv::imread("images/train.png", CV_LOAD_IMAGE_GRAYSCALE);
	std::vector<Shape> shapes;
	
	if (source.empty()) {
		printf("Unable to read input file (%s, %d).", __FILE__, __LINE__);
	}

	viewImage(source, "Original image");

	cv::Mat thresholded = cv::Mat::zeros(source.rows, source.cols, CV_8UC1);
	uchar thresholding_value = 127;
	treshold(source, thresholded, thresholding_value);
	viewImage(thresholded, "Thresholded image");

	cv::Mat indexed = cv::Mat::zeros(source.rows, source.cols, CV_8UC1);
	indexObjects(thresholded, indexed, shapes);
	
	cv::Mat colorized = cv::Mat::zeros(source.rows, source.cols, CV_8UC3);
	colorObjects(indexed, colorized, shapes);
	viewImage(colorized, "Colorized indexed image");

	moments(indexed, shapes);

	for (auto &shape : shapes) {
		std::cout << shape << std::endl;
	}

	cv::waitKey(0);
	return 0;
}	
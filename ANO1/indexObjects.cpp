#include "stdafx.h"
#include "shape.h"

void checkIndex(cv::Mat &thresholded, cv::Mat &indexed, cv::Mat &checked, cv::Point original);
void toIndex(cv::Mat &thresholded, cv::Mat &indexed, cv::Point target, cv::Point original);
void move(cv::Mat &thresholded, cv::Mat &indexed, cv::Mat &checked, cv::Point point);
int usedIndex = 1;

void indexObjects(cv::Mat &thresholded, cv::Mat &indexed, std::vector<Shape> &shapes)
{
	cv::Mat checked = cv::Mat::zeros(thresholded.rows, thresholded.cols, CV_8UC1);

	cv::Vec3b colors[] = { cv::Vec3b(240, 240, 0), cv::Vec3b(120, 240, 0), cv::Vec3b(0, 240, 0), cv::Vec3b(240, 120, 0), cv::Vec3b(0, 0, 240), cv::Vec3b(0, 240, 120),
		cv::Vec3b(0, 240, 240), cv::Vec3b(0, 120, 240), cv::Vec3b(240, 0, 240), cv::Vec3b(240, 0, 120), cv::Vec3b(240, 0, 0), cv::Vec3b(120, 0, 240) };
	
	for (int y = 1; y < thresholded.rows - 1; y++)
	{
		for (int x = 1; x < thresholded.cols - 1; x++)
		{
			cv::Point point = cv::Point(x, y);

			if (thresholded.at<uchar>(point) != 0 && checked.at<uchar>(point) == 0)
			{
				indexed.at<uchar>(point) = usedIndex;

				uchar color = indexed.at<uchar>(point);
				if (color > 12)
				{
					color = 1;
				}

				shapes.push_back(Shape{ usedIndex, colors[color - 1] });

				usedIndex++;

				checkIndex(thresholded, indexed, checked, point);
			}
		}
	}
}

void checkIndex(cv::Mat &thresholded, cv::Mat &indexed, cv::Mat &checked, cv::Point original)
{
	if (original.x == 0 || original.x == thresholded.cols || original.y == 0 || original.y == thresholded.rows || checked.at<uchar>(original) == 1)
	{
		return;
	}

	checked.at<uchar>(original) = 1;

	if (thresholded.at<uchar>(original) != 0)
	{
		toIndex(thresholded, indexed, cv::Point(original.x + 1, original.y + 1), original);
		toIndex(thresholded, indexed, cv::Point(original.x, original.y + 1), original);
		toIndex(thresholded, indexed, cv::Point(original.x - 1, original.y + 1), original);
		toIndex(thresholded, indexed, cv::Point(original.x - 1, original.y), original);
		toIndex(thresholded, indexed, cv::Point(original.x + 1, original.y), original);
		toIndex(thresholded, indexed, cv::Point(original.x - 1, original.y - 1), original);
		toIndex(thresholded, indexed, cv::Point(original.x, original.y - 1), original);
		toIndex(thresholded, indexed, cv::Point(original.x + 1, original.y - 1), original);

		move(thresholded, indexed, checked, cv::Point(original.x + 1, original.y + 1));
		move(thresholded, indexed, checked, cv::Point(original.x, original.y + 1));
		move(thresholded, indexed, checked, cv::Point(original.x - 1, original.y + 1));
		move(thresholded, indexed, checked, cv::Point(original.x - 1, original.y));
		move(thresholded, indexed, checked, cv::Point(original.x + 1, original.y));
		move(thresholded, indexed, checked, cv::Point(original.x - 1, original.y - 1));
		move(thresholded, indexed, checked, cv::Point(original.x, original.y - 1));
		move(thresholded, indexed, checked, cv::Point(original.x + 1, original.y - 1));

	}
}

void toIndex(cv::Mat &thresholded, cv::Mat &indexed, cv::Point target, cv::Point original)
{
	if (thresholded.at<uchar>(target) != 0)
	{
		uchar foundedColor = indexed.at<uchar>(target);
		if (foundedColor != 0)
		{
			indexed.at<uchar>(original) = foundedColor;
		}
	}
}

void move(cv::Mat &thresholded, cv::Mat &indexed, cv::Mat &checked, cv::Point point)
{
	if (thresholded.at<uchar>(point) != 0 && indexed.at<uchar>(point) == 0 && checked.at<uchar>(point) == 0)
	{
		checkIndex(thresholded, indexed, checked, point);
	}
}
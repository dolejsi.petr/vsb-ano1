#pragma once
#include "stdafx.h"
#ifndef   UTILS_H
#define   UTILS_H
#define PI 3.1415926

static void viewImage(cv::Mat &source, const char* name)
{
	int imageHeightUtils = 300;
	int imageWidthUtils = 300;

	cv::namedWindow(name, 0);
	cv::imshow(name, source);
	cv::resizeWindow(name, imageWidthUtils, imageHeightUtils);
}

static void viewImage(cv::Mat &source, const char* name, int height, int width)
{
	cv::namedWindow(name, 0);
	cv::imshow(name, source);
	cv::resizeWindow(name, width, height);
}

static bool isBlack(cv::Vec3b point)
{
	if (point[0] == 0 && point[1] == 0 && point[2] == 0)
	{
		return true;
	}
	return false;
}

#endif
#include "stdafx.h"
#include "shape.h"

double moment(int x, int y, int p, int q);

void moments(cv::Mat &indexed, std::vector<Shape> &shapes)
{
	for (int y = 0; y < indexed.rows; y++) {
		for (int x = 0; x < indexed.cols; x++) {
			uchar v = indexed.at<uchar>(y, x);

			if (v > 0) {
				shapes[v - 1].m00 += moment(x, y, 0, 0);
				shapes[v - 1].m01 += moment(x, y, 0, 1);
				shapes[v - 1].m10 += moment(x, y, 1, 0);
			}
		}
	}

	for (auto& shape : shapes) {
		shape.center = cv::Point(
			static_cast<int>(shape.m10 / shape.m00),
			static_cast<int>(shape.m01 / shape.m00)
		);
	}
}

double moment(int x, int y, int p, int q)
{
	return pow(x, p) * pow(y, q);
}
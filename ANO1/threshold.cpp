#include "stdafx.h"

void treshold(cv::Mat &source, cv::Mat &thresholded, uchar thresholding_value)
{
	for (int y = 0; y < source.rows; y++)
	{
		for (int x = 0; x < source.cols; x++)
		{
			if (source.at<uchar>(y, x) > thresholding_value)
			{
				thresholded.at<uchar>(y, x) = 255;
			}
		}
	}
}
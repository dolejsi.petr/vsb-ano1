#pragma once
class Shape
{
public:
	int id;
	cv::Vec3b color;

	double m00;
	double m01;
	double m10;

	cv::Point center;

	friend std::ostream &operator<<(std::ostream &os, const Shape &shape);
};


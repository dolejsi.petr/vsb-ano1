#include "stdafx.h"
#include "shape.h"

void colorObjects(cv::Mat &indexed, cv::Mat &colorized, std::vector<Shape> &shapes)
{
	for (int y = 1; y < indexed.rows - 1; y++)
	{
		for (int x = 1; x < indexed.cols - 1; x++)
		{
			cv::Point point = cv::Point(x, y);

			if (indexed.at<uchar>(point) != 0)
			{
				uchar color = indexed.at<uchar>(point);
				colorized.at<cv::Vec3b>(point) = shapes[color - 1].color;
			}
		}
	}
}
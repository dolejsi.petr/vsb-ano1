#include "stdafx.h"
#include "shape.h"

std::ostream &operator<<(std::ostream &os, const Shape &shape) {
	os << "Shape: " << shape.id << std::endl
		<< "Color: " << shape.color << std::endl
		<< "Moments: " << std::endl
		<< "  m00: " << shape.m00 << std::endl
		<< "  m01: " << shape.m01 << std::endl
		<< "  m10: " << shape.m10 << std::endl
		<< "Center: " << shape.center << std::endl;
		/*<< "  u00: " << shape.moments.u00 << std::endl
		<< "  u11: " << shape.moments.u11 << std::endl
		<< "  u02: " << shape.moments.u02 << std::endl
		<< "  u20: " << shape.moments.u20 << std::endl
		<< "Features: " << std::endl
		<< "  area: " << shape.features.area << std::endl
		<< "  center: " << shape.features.center << std::endl
		<< "  perimeter: " << shape.features.perimeter << std::endl
		<< "  F1: " << shape.features.F1 << std::endl
		<< "  F2: " << shape.features.F2 << std::endl;*/

	return os;
}